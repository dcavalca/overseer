from chalice import Chalice, Cron
from datetime import datetime
import boto3

app = Chalice(app_name="overseer")


def getBuildver():
    return datetime.now().strftime("%Y%m%d%H%M")


def submitBuild(release, target, buildver=None):
    tags = {
        "Name": f"b-{release}-{target}",
        "BuildRelease": release,
        "BuildTarget": target,
        "TerminateAfterMinutes": 60,
    }
    if buildver:
        tags["Buildver"] = buildver
        tags["Name"] = f"b-{release}-{target}-{buildver}"

    ec2_tags = []
    for k, v in tags.items():
        ec2_tags.append({"Key": k, "Value": str(v)})

    ec2 = boto3.client("ec2")
    instances = ec2.run_instances(
        LaunchTemplate={"LaunchTemplateName": "__EC2_TEMPLATE__", "Version": "$Latest"},
        TagSpecifications=[{"ResourceType": "instance", "Tags": ec2_tags}],
        MinCount=1,
        MaxCount=1,
    )

    return instances["Instances"][0]


def submitBuilds():
    RELEASES = ["38", "39", "rawhide"]
    TARGETS = ["gnome", "kde", "server", "minimal"]

    buildver = getBuildver()

    ret = []

    for release in RELEASES:
        for target in TARGETS:
            instance = submitBuild(release, target, buildver)
            ret.append(
                {
                    "name": f"fedora-{release}-{target}-{buildver}",
                    "release": release,
                    "target": target,
                    "buildver": buildver,
                    "builder": instance["InstanceId"],
                }
            )

    return ret


# Every day at 4pm UTC
@app.schedule(Cron(0, 16, "*", "*", "?", "*"))
def autobuild(event):
    submitBuilds()


# Submit a manual build
@app.route("/build/{release}/{target}", api_key_required=True)
def build(release, target):
    buildver = getBuildver()
    instance = submitBuild(release, target, buildver)
    ret = {
        "name": f"fedora-{release}-{target}-{buildver}",
        "release": release,
        "target": target,
        "buildver": buildver,
        "builder": instance["InstanceId"],
    }

    return ret


# Submit a manual build for all releases and targets
@app.route("/buildall", api_key_required=True)
def buildall():
    return submitBuilds()
